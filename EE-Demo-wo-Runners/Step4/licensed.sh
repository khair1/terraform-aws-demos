#!/bin/bash
url_initial=https://`cat url.txt`
read -p "Enter your the GitLab demo URL [$url_initial]: " url
url=${url:-$url_initial}

read -p "token [required]:" token
token=${token:-required}

read -p "local license [./License_Files/ultimate.txt]:" license
license=${license:-./License_Files/ultimate.txt}

license_file=`cat $license`

echo Here we go...

curl --verbose --request POST --header "PRIVATE-TOKEN:$token" \
"$url/api/v4/license?license=$license_file"
