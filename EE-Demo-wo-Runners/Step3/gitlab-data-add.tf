# Configure the GitLab Provider
provider "gitlab" {
    token = "${var.gitlab_token}"
    base_url = "https://${var.gitlab_url}/api/v4"
}

resource "gitlab_user" "dev1" {
  name             = "Developer One"
  username         = "dev1"
  password         = "password12"
  email            = "dev1@${var.gitlab_url}"
  is_admin         = false
  can_create_group = true
  is_external      = false
}

resource "gitlab_user" "dev2" {
  name             = "Developer Two"
  username         = "dev2"
  password         = "password12"
  email            = "dev2@${var.gitlab_url}"
  is_admin         = false
  can_create_group = true
  is_external      = false
}

resource "gitlab_user" "maint1" {
  name             = "Maintainer 1"
  username         = "maint1"
  password         = "password12"
  email            = "maint1@${var.gitlab_url}"
  is_admin         = false
  can_create_group = true
  is_external      = false
}

resource "gitlab_user" "GodMode" {
  name             = "God Mode"
  username         = "godmode"
  password         = "password12"
  email            = "godmode@${var.gitlab_url}"
  is_admin         = true
  can_create_group = true
  is_external      = false
}

resource "gitlab_group" "TopLevel" {
  name        = "Organization"
  path        = "Organization"
  description = "Organization Top Level"
}

resource "gitlab_group" "ProjectX" {
  name        = "ProjectX"
  parent_id   = "${gitlab_group.TopLevel.id}"
  path        = "ProjectX"
  description = "Special Project X"
}

resource "gitlab_group" "ProjectXDevs" {
  name        = "ProjectX Devs"
  parent_id   = "${gitlab_group.ProjectX.id}"
  path        = "ProjectXDevs"
  description = "Developers for Project X"
}

resource "gitlab_group" "ProjectY" {
  name        = "ProjectY"
  parent_id   = "${gitlab_group.TopLevel.id}"
  path        = "ProjectY"
  description = "Special Project Y"
}

resource "gitlab_group" "ProjectYDevs" {
  name        = "ProjectY Devs"
  parent_id   = "${gitlab_group.ProjectY.id}"
  path        = "ProjectYDevs"
  description = "Developers for Project Y"
}

resource "gitlab_group" "NewBigThing" {
  name        = "New Big Thing"
  parent_id   = "${gitlab_group.TopLevel.id}"
  path        = "NewBigThing"
  description = "New Big Project"
}

resource "gitlab_group" "NewBigThingDevs" {
  name        = "New Big Thing Devs"
  parent_id   = "${gitlab_group.NewBigThing.id}"
  path        = "NewBigThingDevs"
  description = "Developers for New Big Thing Project"
}

resource "gitlab_project" "ProjectY-Special" {
  name        = "Special Project"
  description = "My awesome codebase"
  namespace_id = "${gitlab_group.ProjectYDevs.id}"

  visibility_level = "private"
}

resource "gitlab_project_membership" "ProjectY-Dev2" {
project_id = "${gitlab_project.ProjectY-Special.id}"
user_id = "${gitlab_user.dev2.id}"
access_level = "developer"
}

resource "gitlab_project_membership" "ProjectY-Maint1" {
project_id = "${gitlab_project.ProjectY-Special.id}"
user_id = "${gitlab_user.maint1.id}"
access_level = "master"
}

# provisioner "local-exec" {
#   command = "${var.gitlab_token}" > ../Step4/access_token"
# }
#
# provisioner "local-exec" {
#   command = "${var.gitlab_url}" > ../Step4/site_url"
# }
