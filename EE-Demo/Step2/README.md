``ansible-playbook -i inventory ansible-https.yml --private-key=~/.ssh/cs-demo.pem``

No changes are needed to run this script. However, you must have Ansible installed locally & you _must specify_ the SSH private key used in Step1.

Inventory file is automatically created in Step1 by Terraform.

**NB - This is a hack & will be removed once this demo is refactored to use ELB on AWS. The SSL cert will reside there and this step will be removed**
