``terraform apply -var 'gitlab_token=<admin-api-access-token>' -var 'gitlab_url=<fully qualified domain name - i.e gitlab.com>'``

Alternatively, the Variables.tf file can be updated with the Fully Qualified Domain Name (FQDN) & API acccess token. 