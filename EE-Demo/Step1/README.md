In the Variables.tf file, ensure the path to the SSH private key is setup correctly:

```
variable "private_ssh_key_dir" {
  default = "/Users/kelly/.ssh/cs-demo.pem"
  description = "Location of the SSH private key provisioning infrastructure."
}
```

_IF_ this is the first time you are running then you will need to initialize the Terraform state & helpers by typing:

```terraform init```

Once this is set, you can bring up the infrastructure using this command.

```terraform apply```


The demo relies on AMIs available in all regions for the GitLab CS organization. If you are not a part of this organization but want to reuse then please see the AMIs created by https://gitlab.com/khair1/packer-ami-builder
