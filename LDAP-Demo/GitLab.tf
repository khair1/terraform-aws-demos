
data "aws_ami" "GitLab-EE" {
  most_recent      = true

  filter {
    name   = "name"
    values = ["*KH-GitLab-EE*"]
  }

  filter {
    name = "tag:Button_Pusher"
    values =  ["*khair@gitlab.com*"]
  }
  #
  # name_regex = "^ami-\\d{3}"
  owners     =  ["self"]
}

resource "aws_instance" "GitLabEE" {
  ami =  "${data.aws_ami.GitLab-EE.id}"
  instance_type = "${var.GitLabEE_EC2_instance_type}"
  vpc_security_group_ids = [
    "${aws_security_group.HTTP_HTTPS_SSH.id}",
    "${aws_security_group.Allow_ICMP_GL.id}"
    ]
  key_name = "${var.ssh_key}"
  tags {
      Name = "${var.GitLab_friendly_name}"
    }


    provisioner "file" {
      source      = "scripts/reconfigure-GitLab-EE.sh"
      destination = "/tmp/part1.sh"
    }

    provisioner "file" {
      source = "data/gitlabrb-ldap"
      destination = "/tmp/gitlabrb-ldap"
    }

   provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/part1.sh",
      "sudo /tmp/part1.sh"
      ]
   }

   connection {
   user = "ubuntu"
   private_key = "${file("${var.private_ssh_key_dir}")}"
   }
}
