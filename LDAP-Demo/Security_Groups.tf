resource "aws_security_group" "HTTP_HTTPS_SSH" {
  name = "${var.SG-Prefix}http_https_ssh-${timestamp()}"
  tags {
        Name = "${var.SG-Prefix}http_https_ssh-${timestamp()}"
  }
  description = "HTTP, HTTPS & SSH CONNECTIONS INBOUND (managed by Terraform)"

  vpc_id = "${data.aws_vpc.default.id}"

  ingress {
         from_port = 443
         to_port = 443
         protocol = "TCP"
         cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
       from_port = 80
       to_port = 80
       protocol = "TCP"
       cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "SSH_Only" {
  name = "${var.SG-Prefix}ssh-only-${timestamp()}"
  tags {
        Name =  "${var.SG-Prefix}ssh-only-${timestamp()}"
  }

  description = "ONLY SSH CONNECTIONS INBOUND (managed by Terraform)"
  vpc_id = "${data.aws_vpc.default.id}"


  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "Allow_LDAP" {
  name = "${var.SG-Prefix}allow_LDAP-${timestamp()}"
  tags {
        Name =  "${var.SG-Prefix}allow_LDAP-${timestamp()}"
  }

  description = "Allow LDAP connections (managed by Terraform)"
  vpc_id = "${data.aws_vpc.default.id}"


  ingress {
    from_port   = "389"
    to_port     = "389"
    protocol    = "TCP"
    cidr_blocks = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  }
  ingress {
    from_port   = "636"
    to_port     = "636"
    protocol    = "TCP"
    cidr_blocks = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "Allow_ICMP_GL" {
  name = "${var.SG-Prefix}allow_ICMP_GL-${timestamp()}"
  tags {
      Name =  "${var.SG-Prefix}allow_ICMP_GL-${timestamp()}"
      }

description = "Allow ICMP connections (managed by Terraform)"
vpc_id = "${data.aws_vpc.default.id}"


ingress {
  protocol = "icmp"
  from_port = -1
  to_port = -1
#  cidr_blocks = ["${aws_instance.OpenLDAP.private_ip}/32"]
  }
egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "Allow_ICMP_LDAP" {
  name = "${var.SG-Prefix}allow_ICMP_LDAP-${timestamp()}"
  tags {
      Name =  "${var.SG-Prefix}allow_ICMP_LDAP-${timestamp()}"
      }

description = "Allow ICMP connections (managed by Terraform)"
vpc_id = "${data.aws_vpc.default.id}"


ingress {
  protocol = "icmp"
  from_port = -1
  to_port = -1
  cidr_blocks = ["${aws_instance.GitLabEE.private_ip}/32"]
  }
egress {
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}
